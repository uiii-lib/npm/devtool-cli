import path from "path";
import pc from "picocolors";
import { Formatter } from "picocolors/types.js";

import { note } from "@uiii-lib/prompts";
import { kebabCase } from "@uiii-lib/utils";

export interface GeneratorResultItem {
	file: string;
	result: "CREATED" | "MODIFIED" | "EXISTS";
}

export interface GeneratorInputItem {
	file: string;
	label?: string;
}

const resultColors: Record<GeneratorResultItem["result"], Formatter> = {
	CREATED: pc.green,
	MODIFIED: pc.magenta,
	EXISTS: pc.red,
};

export class GeneratorRunContext {
	inputFileItems: GeneratorInputItem[] = [];
	resultItems: GeneratorResultItem[] = [];

	constructor(
		public readonly params: Record<string, any>,
		public templatesDirpath: string,
		public targetDirpath: string,
	) {}

	addInputFile(file: string, label?: string) {
		this.inputFileItems.push({ file, label });
	}

	addResult(file: string, result: GeneratorResultItem["result"]) {
		const existingItem = this.resultItems.find((it) => it.file === file);
		if (existingItem) {
			throw new Error(
				`Output already added: ${existingItem.result} ${existingItem.file}`,
			);
		}

		this.resultItems.push({ file, result });
	}

	getInputFiles() {
		return this.inputFileItems.map((it) => it.file);
	}

	getInputFileByLabel(label: string) {
		const item = this.inputFileItems.find((it) => it.label === label);

		if (!item) {
			throw new Error(`No input file with label: ${label}`);
		}

		return item.file;
	}

	getTargetFilepath(inputFile: string) {
		const target = inputFile
			.replace(/__([^_]+)__/g, (_, paramName) => {
				return kebabCase(this.params[paramName]);
			})
			.replace(".ejs", "");

		return path.join(this.targetDirpath, target);
	}

	printOutput() {
		note(
			this.resultItems
				.map((it) =>
					resultColors[it.result](
						`${it.result} ${path.relative(process.cwd(), it.file)}`,
					),
				)
				.join("\n") || pc.dim("No files were created or modified"),
			"Result",
		);
	}

	clearOutput() {
		this.resultItems = [];
	}
}
