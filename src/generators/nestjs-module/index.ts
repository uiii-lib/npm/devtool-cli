import { isCancel, log, text } from "@uiii-lib/prompts";
import { pascalCase } from "@uiii-lib/utils";

import { fileExists } from "../../helpers/file-exists.js";
import { BaseGenerator } from "../base.generator.js";
import { GeneratorRunContext } from "../generator-context.js";

export interface NestjsModuleGeneratorParams {
	name: string;
}

export class NestjsModuleGenerator extends BaseGenerator<NestjsModuleGeneratorParams> {
	constructor() {
		super("nestjs-module");
	}

	protected async getParams(params: Partial<NestjsModuleGeneratorParams> = {}) {
		const name =
			params.name ||
			(await text({
				message: "Module name",
				validate(value) {
					if (value.length === 0) {
						return "Module name is required";
					}
				},
			}));

		if (isCancel(name)) {
			process.exit();
		}

		return {
			name,
		};
	}

	protected async initContext(context: GeneratorRunContext) {
		context.addInputFile("src/__name__/__name__.module.ts.ejs", "module");
	}

	protected async shouldGenerate(
		context: GeneratorRunContext,
	): Promise<boolean> {
		const moduleExists = await fileExists(
			context.getTargetFilepath(context.getInputFileByLabel("module")),
		);

		if (moduleExists) {
			log.info(`Module ${pascalCase(context.params.name)} already exists`);
		}

		return !moduleExists;
	}

	protected async generate(context: GeneratorRunContext) {
		await this.step(
			`Generating files for module ${pascalCase(context.params.name)}`,
			async () => {
				return await this.generateFiles(context);
			},
			context,
		);
	}
}
