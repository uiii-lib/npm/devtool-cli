import fs from "fs/promises";
import path from "path";

import {
	confirm,
	group,
	log,
	multiselect,
	select,
	tasks,
	text,
} from "@uiii-lib/prompts";
import { camelCase, kebabCase, pascalCase } from "@uiii-lib/utils";

import { addImportDeclaration } from "../../helpers/add-import-declaration.js";
import { addModuleMetadataDeclaration } from "../../helpers/add-module-metadata-declaration.js";
import { fileExists } from "../../helpers/file-exists.js";
import { prettify } from "../../helpers/prettify.js";
import { saveFile } from "../../helpers/save-file.js";
import { BaseGenerator } from "../base.generator.js";
import { GeneratorRunContext } from "../generator-context.js";
import { NestjsModuleGenerator } from "../nestjs-module/index.js";

export interface NestjsResourceGeneratorParams {
	name: string;
	module: string;
	dbType: string;
	generateId: "objectId" | "counter" | false;
	addController: boolean;
	controllerEndpoints: ("getMany" | "getOne")[];
}

export class NestjsResourceGenerator extends BaseGenerator<NestjsResourceGeneratorParams> {
	constructor() {
		super("nestjs-resource");
	}

	protected async getParams(
		params: Partial<NestjsResourceGeneratorParams> = {},
	): Promise<NestjsResourceGeneratorParams> {
		return (await group(
			{
				name: async () =>
					params.name ||
					(await text({
						message: "Resource name",
						validate(value) {
							if (value.length === 0) {
								return "Resource name is required";
							}
						},
					})),
				module: async ({ results }) =>
					params.module ||
					(await text({
						message: "Module name",
						placeholder: results.name || "",
						defaultValue: results.name || "",
					})),
				dbType: async () =>
					params.dbType ||
					(await select({
						message: "Database type",
						initialValue: "mongo",
						options: [
							{ value: "mongo", label: "MongoDB" },
							{ value: "postgres", label: "PostgreSQL" },
						],
					})),
				generateId: async ({ results }) => {
					if (results.dbType !== "mongo") {
						return false;
					}

					return (
						params.generateId ||
						(await select<NestjsResourceGeneratorParams["generateId"]>({
							message: "Generate ID?",
							initialValue: "objectId",
							options: [
								{ value: "objectId", label: "ObjectID" },
								{ value: "counter", label: "Counter" },
								{ value: false, label: "No, must be specified manually" },
							],
						}))
					);
				},
				addController: async () =>
					params.addController ||
					(await confirm({
						message: "Add controller?",
						active: "Yes",
						inactive: "No",
						initialValue: true,
					})),
				controllerEndpoints: async ({ results }) => {
					if (!results.addController) {
						return [];
					}

					return (
						params.controllerEndpoints ||
						(await multiselect<
							NestjsResourceGeneratorParams["controllerEndpoints"][number]
						>({
							message: "Controller endpoints",
							options: [{ value: "getMany" }, { value: "getOne" }],
							initialValues: ["getMany", "getOne"],
						}))
					);
				},
			},
			{
				onCancel: () => {
					process.exit();
				},
			},
		)) as NestjsResourceGeneratorParams;
	}

	protected async initContext(context: GeneratorRunContext) {
		context.templatesDirpath = path.join(
			this.generatorFilesDirpath,
			context.params.dbType,
		);

		context.addInputFile("src/__module__/model/__name__.ts.ejs");
		context.addInputFile(
			"src/__module__/__name__.repository.ts.ejs",
			"repository",
		);

		if (context.params.addController) {
			context.addInputFile(
				"src/__module__/__name__.controller.ts.ejs",
				"repository",
			);
		}
	}

	protected async shouldGenerate(
		context: GeneratorRunContext,
	): Promise<boolean> {
		const resourceExists = await fileExists(
			context.getTargetFilepath(context.getInputFileByLabel("repository")),
		);

		if (resourceExists) {
			log.info(`Resource ${pascalCase(context.params.name)} already exists`);
		}

		return !resourceExists;
	}

	async generate(context: GeneratorRunContext) {
		await this.runGenerator<NestjsModuleGenerator>("nestjs-module", {
			name: context.params.module,
		});

		await this.step(
			`Generating files for resource ${pascalCase(context.params.name)}`,
			async () => {
				return await this.generateFiles(context);
			},
			context,
		);

		await this.step(
			`Adding resource ${pascalCase(context.params.name)} to module ${pascalCase(context.params.module)}`,
			async () => {
				const moduleFilepath = context.getTargetFilepath(
					"src/__module__/__module__.module.ts",
				);
				let moduleContent = await fs.readFile(moduleFilepath, "utf-8");

				moduleContent = addImportDeclaration(
					moduleContent,
					`${pascalCase(context.params.name)}Repository`,
					`./${kebabCase(context.params.name)}.repository.js`,
				);
				moduleContent = addModuleMetadataDeclaration(
					moduleContent,
					"providers",
					`${pascalCase(context.params.name)}Repository`,
				);
				moduleContent = addModuleMetadataDeclaration(
					moduleContent,
					"exports",
					`${pascalCase(context.params.name)}Repository`,
				);

				if (context.params.addController) {
					moduleContent = addImportDeclaration(
						moduleContent,
						`${pascalCase(context.params.name)}Controller`,
						`./${kebabCase(context.params.name)}.controller.js`,
					);
					moduleContent = addModuleMetadataDeclaration(
						moduleContent,
						"controllers",
						`${pascalCase(context.params.name)}Controller`,
					);
				}

				moduleContent = await prettify(moduleContent, moduleFilepath);

				await saveFile(moduleFilepath, moduleContent);

				context.resultItems.push({
					result: "MODIFIED",
					file: moduleFilepath,
				});
			},
			context,
		);
	}
}
