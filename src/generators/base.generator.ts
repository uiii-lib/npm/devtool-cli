import fs from "fs/promises";
import path from "path";
import ejs from "ejs";

import { log, tasks } from "@uiii-lib/prompts";
import { camelCase, kebabCase, pascalCase } from "@uiii-lib/utils";

import { fileExists } from "../helpers/file-exists.js";
import { getRootPackageJson } from "../helpers/package-json.js";
import { prettify } from "../helpers/prettify.js";
import { saveFile } from "../helpers/save-file.js";
import { walkDir } from "../helpers/walk-dir.js";

import { GeneratorRunContext } from "./generator-context.js";

export abstract class BaseGenerator<Params extends object> {
	protected generatorFilesDirpath: string;
	protected generators: Record<string, BaseGenerator<any>> = {};

	constructor(public readonly name: string) {
		const packageJson = getRootPackageJson(import.meta.dirname);
		this.generatorFilesDirpath = path.join(
			packageJson.cwd,
			"src",
			"generators",
			name,
			"files",
		);
	}

	register(collection: Record<string, BaseGenerator<any>>) {
		collection[this.name] = this;
		this.generators = collection;
	}

	async run(params?: Params) {
		const resolvedParams = await this.getParams(params);

		const context = new GeneratorRunContext(
			resolvedParams,
			this.generatorFilesDirpath,
			process.cwd(),
		);

		await this.initContext(context);

		if (await this.shouldGenerate(context)) {
			await this.generate(context);
		}
	}

	protected abstract getParams(params?: Partial<Params>): Promise<Params>;

	protected async initContext(context: GeneratorRunContext): Promise<void> {}

	protected async shouldGenerate(
		context: GeneratorRunContext,
	): Promise<boolean> {
		return true;
	}

	protected abstract generate(context: GeneratorRunContext): Promise<void>;

	protected async generateFiles(context: GeneratorRunContext) {
		for await (const inputFile of context.getInputFiles()) {
			const templateFilepath = path.join(context.templatesDirpath, inputFile);

			if (!(await fs.stat(templateFilepath)).isFile()) {
				continue;
			}

			const targetFilepath = context.getTargetFilepath(inputFile);

			if (await fileExists(targetFilepath)) {
				context.addResult(
					path.relative(process.cwd(), targetFilepath),
					"EXISTS",
				);
			} else {
				const targetContent = await this.renderFile(
					templateFilepath,
					targetFilepath,
					context,
				);

				await saveFile(targetFilepath, targetContent);

				context.addResult(
					path.relative(process.cwd(), targetFilepath),
					"CREATED",
				);
			}
		}
	}

	protected async renderFile(
		templateFilepath: string,
		targetFilepath: string,
		context: GeneratorRunContext,
	) {
		const targetContent = ejs.render(
			await fs.readFile(templateFilepath, "utf-8"),
			{
				params: context.params,
				camelCase,
				pascalCase,
				kebabCase,
			},
		);

		return await prettify(targetContent, targetFilepath);
	}

	protected async step(
		title: string,
		fn: () => Promise<void>,
		context: GeneratorRunContext,
	) {
		await tasks([
			{
				title,
				task: async () => {
					try {
						await fn();
					} catch (e: any) {
						log.error(e.message);
						throw e;
					}
				},
			},
		]);

		context.printOutput();
		context.clearOutput();
	}

	protected async runGenerator<T>(
		generatorName: string,
		params: T extends BaseGenerator<infer P> ? P : any,
	) {
		const generator = this.generators[generatorName];

		if (!generator) {
			throw new Error(
				`Generator '${generatorName}' is not registered in the collection`,
			);
		}

		await generator.run(params);
	}
}
