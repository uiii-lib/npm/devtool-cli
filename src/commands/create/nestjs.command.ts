import fs from "fs";
import path from "path";
import { Command } from "commander";
import { glob } from "glob";

import { convertEolToLf } from "../../helpers/convert-eol-to-lf.js";
import { copyAssets } from "../../helpers/copy-assets.js";
import { getAssetsPath } from "../../helpers/get-assets-path.js";
import { setPackageJsonValue } from "../../helpers/package-json.js";
import { spawn } from "../../helpers/spawn.js";
import { GenerateCommand } from "../generate.command.js";

interface CreateNestJsCommandOptions {}

export class CreateNestJsCommand {
	static register(program: Command) {
		program
			.command("nestjs")
			.description("create a NestJS project")
			.argument("<name>", `name of the project`)
			.argument(
				"[directory]",
				`directory where to create the project (relative to cwd, default: <name>)`,
			)
			.action(CreateNestJsCommand.run);
	}

	static async run(
		name: string,
		directory: string = name,
		options: CreateNestJsCommandOptions = {},
	) {
		const projectDir = path.join(process.cwd(), directory);

		await CreateNestJsCommand.install(name, directory, projectDir);
		await CreateNestJsCommand.clean(projectDir);
		await CreateNestJsCommand.normalizeEol(projectDir);

		// Set project to ES module
		setPackageJsonValue(projectDir, "type", "module", "version");

		// Rename current tsconfig.json (will extended from it)
		fs.renameSync(
			path.join(projectDir, "tsconfig.json"),
			path.join(projectDir, "tsconfig.base.json"),
		);

		await copyAssets(path.join(getAssetsPath("create"), "nestjs"), projectDir);

		// Generate starter files
		await GenerateCommand.run("nestjs-entry", [directory, "main"], {});
	}

	private static async install(
		name: string,
		directory: string,
		projectDir: string,
	) {
		await spawn(
			"npx",
			[
				"@nestjs/cli",
				"new",
				`--directory ${directory}`,
				`--skip-git`,
				`-p npm`,
				`--strict`,
				name,
			],
			{ stdio: "inherit", shell: true },
		);

		const packages = [
			"@uiii-lib/eslint-config",
			"@uiii-lib/prettier-config",
			"@uiii-lib/tsconfig",
		];

		await spawn("npm", ["i", ...packages], {
			cwd: projectDir,
			stdio: "inherit",
			shell: true,
		});
	}

	private static async clean(projectDir: string) {
		for (const file of fs.readdirSync(path.join(projectDir, "src"))) {
			fs.unlinkSync(path.join(projectDir, "src", file));
		}

		fs.unlinkSync(path.join(projectDir, ".eslintrc.js"));
	}

	private static async normalizeEol(projectDir: string) {
		const files = await glob("**/*", {
			cwd: projectDir,
			ignore: "node_modules/**",
			nodir: true,
			dot: true,
		});

		for (const file of files) {
			await convertEolToLf(path.join(projectDir, file));
		}
	}
}
