import fs from "fs";
import path from "path";
import { Command } from "commander";

import { copyAssets } from "../../helpers/copy-assets.js";
import { getAssetsPath } from "../../helpers/get-assets-path.js";
import { loadJson, saveJson } from "../../helpers/json.js";
import { setPackageJsonValue } from "../../helpers/package-json.js";
import { spawn } from "../../helpers/spawn.js";
import { GenerateCommand } from "../generate.command.js";

interface CreateViteCommandOptions {}

export class CreateViteCommand {
	static register(program: Command) {
		program
			.command("vite")
			.description("create a Vite project")
			.argument("<name>", `name of the project`)
			.argument(
				"[directory]",
				`directory where to create the project (relative to cwd, default: <name>)`,
			)
			.action(CreateViteCommand.run);
	}

	static async run(
		name: string,
		directory: string = name,
		options: CreateViteCommandOptions = {},
	) {
		const projectDir = path.join(process.cwd(), directory);

		await CreateViteCommand.install(name, directory, projectDir);
		await CreateViteCommand.clean(projectDir);

		// Set project to ES module
		setPackageJsonValue(projectDir, "type", "module", "version");

		// Rename current tsconfig.json (will extended from it)
		fs.renameSync(
			path.join(projectDir, "tsconfig.json"),
			path.join(projectDir, "tsconfig.old.json"),
		);

		await copyAssets(path.join(getAssetsPath("create"), "vite"), projectDir);

		// Generate starter files
		await GenerateCommand.run("vite-entry", [directory], {});

		await CreateViteCommand.updateTsConfig(projectDir);
	}

	private static async install(
		name: string,
		directory: string,
		projectDir: string,
	) {
		await spawn(
			"npm",
			["create", "vite@latest", directory, `--`, `--template react-ts`],
			{ stdio: "inherit", shell: true },
		);

		await spawn("npm", ["i"], {
			cwd: projectDir,
			stdio: "inherit",
			shell: true,
		});

		const packages = [
			"prettier",
			"@uiii-lib/eslint-config",
			"@uiii-lib/prettier-config",
			"@uiii-lib/tsconfig",
			"eslint-plugin-react",
		];

		await spawn("npm", ["i", ...packages], {
			cwd: projectDir,
			stdio: "inherit",
			shell: true,
		});
	}

	private static async clean(projectDir: string) {
		for (const file of fs.readdirSync(path.join(projectDir, "src"))) {
			fs.rmSync(path.join(projectDir, "src", file), { recursive: true });
		}

		fs.rmSync(path.join(projectDir, ".eslintrc.cjs"));
	}

	private static async updateTsConfig(projectDir: string) {
		const baseTsConfig = loadJson(path.join(projectDir, "tsconfig.app.json"));
		const oldTsConfig = loadJson(path.join(projectDir, "tsconfig.old.json"));
		const tsConfig = loadJson(path.join(projectDir, "tsconfig.json"));

		for (const prop of ["files", "include", "exclude", "references"]) {
			if (prop in baseTsConfig) {
				tsConfig[prop] = baseTsConfig[prop];
				delete baseTsConfig[prop];
			}
		}

		tsConfig.references.push(
			...oldTsConfig.references.filter(
				(it: any) => it.path !== "./tsconfig.app.json",
			),
		);

		saveJson(path.join(projectDir, "tsconfig.app.json"), baseTsConfig);
		saveJson(path.join(projectDir, "tsconfig.json"), tsConfig);

		fs.unlinkSync(path.join(projectDir, "tsconfig.old.json"));
	}
}
