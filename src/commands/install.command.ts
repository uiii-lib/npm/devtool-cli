import { Command } from "commander";

import { spawn } from "../helpers/spawn.js";

interface InstallCommandOptions {}

export class InstallCommand {
	static register(program: Command) {
		program
			.command("install")
			.description(
				"install NPM dependencies (according to package.json, yarn.lock files)",
			)
			.argument("[args...]")
			.allowUnknownOption()
			.addHelpText("after", "  options accepted by 'yarn install'")
			.action(InstallCommand.run);
	}

	static async run(args: string[], options: InstallCommandOptions) {
		await spawn("yarn", ["install", ...args], {
			stdio: "inherit",
			shell: true,
		});
	}
}
