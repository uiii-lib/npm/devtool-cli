import { Command } from "commander";

import { CreateNestJsCommand } from "./create/nestjs.command.js";
import { CreateViteCommand } from "./create/vite.command.js";

interface CreateCommandOptions {}

export class CreateCommand {
	static register(program: Command) {
		const command = program.command("create").description("create a project");

		CreateNestJsCommand.register(command);
		CreateViteCommand.register(command);
	}
}
