import path from "path";
import { Command } from "commander";

import { getStackName } from "../helpers/get-stack-name.js";
import { getRootPackageJson } from "../helpers/package-json.js";
import { spawn } from "../helpers/spawn.js";

interface StopCommandOptions {}

export class StopCommand {
	static register(program: Command) {
		program.command("stop").description("stop app").action(StopCommand.run);
	}

	static async run(options: StopCommandOptions = {}) {
		const packageJson = getRootPackageJson(process.cwd());
		const stackName = getStackName(packageJson);

		await spawn("docker", ["stack", "rm", stackName]);
	}
}
