import { exec } from "child_process";
import fs from "fs";
import path from "path";
import { Command } from "commander";
import YAML from "yaml";

import { getAppDataPath } from "../helpers/get-appdata-path.js";

export class ConfigCommand {
	static readonly knownKeys = ["linkVolume"];

	static register(program: Command) {
		program
			.command("config")
			.description("get or set configuration key")
			.argument("[key]", "configuration key to get or set")
			.argument("[value]", "configuration value to set")
			.action(ConfigCommand.run);
	}

	static run(key?: string, value?: string) {
		if (key) {
			if (value !== undefined) {
				ConfigCommand.setConfig(key, value);
			} else {
				console.log(ConfigCommand.getConfig(key));
			}
		} else {
			ConfigCommand.openConfig();
		}
	}

	static getConfig(key: string) {
		if (!ConfigCommand.knownKeys.includes(key)) {
			console.error(`Unknown config key '${key}'`);
			process.exit(1);
		}

		const config =
			YAML.parse(fs.readFileSync(ConfigCommand.getConfigFilePath(), "utf-8")) ||
			{};
		return config[key];
	}

	static setConfig(key: string, value: string) {
		if (!ConfigCommand.knownKeys.includes(key)) {
			console.error(`Unknown config key '${key}'`);
			process.exit(1);
		}

		const configFilePath = ConfigCommand.getConfigFilePath();
		const config = YAML.parse(fs.readFileSync(configFilePath, "utf-8")) || {};

		config[key] = value;
		fs.writeFileSync(configFilePath, YAML.stringify(config));
	}

	static openConfig() {
		exec(ConfigCommand.getConfigFilePath());
	}

	static getConfigFilePath() {
		const appDataPath = getAppDataPath();
		const configFilePath = path.join(appDataPath, "config.yml");

		if (!fs.existsSync(configFilePath)) {
			fs.closeSync(fs.openSync(configFilePath, "w"));
		}

		return configFilePath;
	}
}
