import { exec as nodeExec } from "child_process";
import path from "path";
import { promisify } from "util";
import { Command } from "commander";

import { sleep } from "@uiii-lib/utils";

import { getStackName } from "../helpers/get-stack-name.js";
import { getRootPackageJson } from "../helpers/package-json.js";
import { spawn } from "../helpers/spawn.js";

const exec = promisify(nodeExec);

interface BashCommandOptions {}

export class BashCommand {
	static register(program: Command) {
		program
			.command("bash")
			.description("execute bash inside a running stack service container")
			.argument("<service>", "Name of the service")
			.argument("[shell]", "Shell to exec", "bash")
			.action(BashCommand.run);
	}

	static async run(
		serviceName: string,
		shell: string,
		options: BashCommandOptions = {},
	) {
		const packageJson = getRootPackageJson(process.cwd());
		const stackName = getStackName(packageJson);
		const fullServiceName = `${stackName}_${serviceName}`;

		while (true) {
			try {
				const { stdout, stderr } = await exec(
					`docker service ps ${fullServiceName} -q | head -1`,
				);
				const containerHash = stdout.trim();

				const containerFullName = `${fullServiceName}.1.${containerHash}`;

				await spawn("docker", ["exec", "-it", containerFullName, shell]);

				// clean exit
				break;
			} catch (e) {
				console.log(e);
			}

			console.error("\nCan't connect to the container, retry in 5 sec ...");
			await sleep(5000);
		}
	}
}
