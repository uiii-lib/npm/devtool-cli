import chokidar from "chokidar";
import { Command } from "commander";
import debounce from "lodash.debounce";

import { spawn } from "../helpers/spawn.js";

interface BuildCommandOptions {}

export class BuildCommand {
	static register(program: Command) {
		program
			.command("build")
			.description(
				"run build watch script and make the package available for linking",
			)
			.argument(
				"[script]",
				`name of the build watch script in package.json to run`,
				"build:watch",
			)
			.action(BuildCommand.run);
	}

	static async run(script: string, options: BuildCommandOptions) {
		const watcher = chokidar.watch(".", {
			ignoreInitial: true,
			ignored: ["**/.git/**", "**/.yalc/**", "**/node_modules/**"],
			usePolling: true,
		});

		const yalcPush = debounce(
			() => spawn("yalc", ["push"], { stdio: "inherit", shell: true }),
			1000,
		);

		watcher.on("all", (event, path) => {
			yalcPush();
		});

		await spawn("npm", ["run", script], { stdio: "inherit", shell: true });
	}
}
