import { Command } from "commander";

import { StartCommand } from "./start.command.js";
import { StopCommand } from "./stop.command.js";

interface RestartCommandOptions {}

export class RestartCommand {
	static register(program: Command) {
		program
			.command("restart")
			.description("restart currently running docker stack")
			.argument("[stack]", "Stack which will be started after restart", "dev")
			.action(RestartCommand.run);
	}

	static async run(stack: string = "dev", options: RestartCommandOptions = {}) {
		await StopCommand.run();
		await StartCommand.run(stack);
	}
}
