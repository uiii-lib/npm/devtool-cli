import fs, { appendFile } from "fs";
import path from "path";
import { Command } from "commander";
import { glob } from "glob";

import { isCancel, multiselect } from "@uiii-lib/prompts";

import { getRootPackageJson } from "../helpers/package-json.js";
import { spawn } from "../helpers/spawn.js";

interface BuildImagesCommandOptions {}

export class BuildImagesCommand {
	static register(program: Command) {
		program
			.command("build-images")
			.description("build docker images")
			.argument(
				"[app dir|dockerfile]",
				`App dir do scan for dockerfiles or a path to a specific dockerfile`,
			)
			.action(BuildImagesCommand.run);
	}

	static async run(
		appDirOrDockerfile: string | undefined,
		options: BuildImagesCommandOptions,
	) {
		const packageJson = getRootPackageJson(process.cwd());

		appDirOrDockerfile =
			appDirOrDockerfile && path.join(packageJson.cwd, appDirOrDockerfile);
		console.log(appDirOrDockerfile);

		const dockerfiles: string[] = [];

		let searchRoot = packageJson.cwd;
		const searchPatters = [];

		if (appDirOrDockerfile && fs.lstatSync(appDirOrDockerfile).isFile()) {
			searchPatters.push(appDirOrDockerfile);
		} else if (appDirOrDockerfile) {
			searchRoot = appDirOrDockerfile;
			searchPatters.push(`.config/docker/{*.Dockerfile,Dockerfile}`);
		} else {
			searchPatters.push(
				".config/docker/{*.Dockerfile,Dockerfile}",
				"**/*/.config/docker/{*.Dockerfile,Dockerfile}",
			);
		}

		dockerfiles.push(
			...(await glob(searchPatters, { cwd: searchRoot, absolute: true })),
		);

		console.log(dockerfiles);

		const choices = [];

		for await (let dockerfile of dockerfiles) {
			console.log("dockerfile", dockerfile);

			dockerfile = path.relative(packageJson.cwd, dockerfile);

			let dockerImageName = packageJson.name.replace("@", "");

			if (!dockerfile.startsWith(".config")) {
				const parts = dockerfile.split(path.sep);
				const appName = parts.slice(0, parts.indexOf(".config")).join("/");
				dockerImageName += `/${appName}`;
			}

			const imageName = path
				.basename(dockerfile)
				.replace(/\.?Dockerfile$/, "")
				.replace(/\./g, "/");

			if (imageName) {
				dockerImageName += `/${imageName}`;
			}

			console.log("docker image", dockerImageName);

			choices.push({
				value: {
					dockerfile,
					dockerImageName,
				},
				label: dockerfile,
				hint: dockerImageName,
			});
		}

		const selectedChoices = await multiselect({
			message: `Select which docker images to build`,
			options: choices,
		});

		if (isCancel(selectedChoices)) {
			process.exit();
		}

		for (const choice of selectedChoices) {
			console.log(
				`:: Building docker image ${choice.dockerImageName}\n:: from ${choice.dockerfile}`,
			);
			await spawn(
				"docker",
				["build", "-t", choice.dockerImageName, "-f", choice.dockerfile, "."],
				{ cwd: packageJson.cwd },
			);
		}
	}
}
