import fs from "fs";
import path from "path";
import { Command } from "commander";
import dotenv from "dotenv";
import YAML from "yaml";

import { confirm, isCancel } from "@uiii-lib/prompts";

import { getBinPath } from "../helpers/get-bin-path.js";
import { getStackName } from "../helpers/get-stack-name.js";
import { getRootPackageJson } from "../helpers/package-json.js";
import { spawn } from "../helpers/spawn.js";

const HOSTS_FILE = "C:\\Windows\\system32\\drivers\\etc\\hosts";

interface StartCommandOptions {}

export class StartCommand {
	static register(program: Command) {
		program
			.command("start")
			.description("start app for development")
			.argument("[stack]", "Stack to run", "dev")
			.action(StartCommand.run);
	}

	static async run(stack: string = "dev", options: StartCommandOptions = {}) {
		const packageJson = getRootPackageJson(process.cwd());
		const stackName = getStackName(packageJson);
		const composeFile = path.join(
			packageJson.cwd,
			".config",
			"compose",
			`${stack}.yml`,
		);

		if (!fs.existsSync(composeFile)) {
			throw new Error(`Compose file ${composeFile} not found`);
		}

		const composeYaml = YAML.parse(fs.readFileSync(composeFile, "utf-8"));
		const hosts = fs.readFileSync(HOSTS_FILE, "utf-8");

		const domainsToHost = [];

		for (const serviceName in composeYaml.services) {
			const service = composeYaml.services[serviceName];

			if (service.env_file) {
				const envFile = path.join(path.dirname(composeFile), service.env_file);
				const env = dotenv.parse(fs.readFileSync(envFile, "utf-8"));
				const domain = env["VIRTUAL_HOST"];

				if (
					domain &&
					hosts.search(
						new RegExp(
							`127\\.0\\.0\\.1\\s+${domain.replace(/[.-_]/g, "\\$&")}`,
						),
					) === -1
				) {
					const addDomain = await confirm({
						message: `Domain '${domain}' is not in /etc/hosts, do you want to add it?`,
						active: "Yes",
						inactive: "No",
						initialValue: true,
					});

					if (isCancel(addDomain)) {
						process.exit();
					}

					if (addDomain) {
						domainsToHost.push(domain);
					}
				}
			}
		}

		if (domainsToHost.length > 0) {
			await spawn(getBinPath("add-etc-hosts.cmd"), domainsToHost, {
				elevate: true,
				stdio: "inherit",
				shell: true,
			});
		}

		await spawn("docker", ["stack", "deploy", "-c", composeFile, stackName]);
	}
}
