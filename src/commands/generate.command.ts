import { Command } from "commander";

import { intro, isCancel, outro, select } from "@uiii-lib/prompts";

import { BaseGenerator } from "../generators/base.generator.js";
import { GeneratorRunContext } from "../generators/generator-context.js";
import { NestjsModuleGenerator } from "../generators/nestjs-module/index.js";
import { NestjsResourceGenerator } from "../generators/nestjs-resource/index.js";

interface GenerateCommandOptions {}

export class GenerateCommand {
	protected static generators: Record<string, BaseGenerator<any>> = {};

	static register(program: Command) {
		program
			.command("generate")
			.alias("g")
			.alias("gen")
			.description("generate code")
			.argument("[generator]", `generator to generate code with`)
			//.argument('[template-args...]', `args passed to schematic`)
			.allowUnknownOption()
			//.addHelpText('after', "  options accepted by schematic")
			.action(GenerateCommand.run);

		new NestjsModuleGenerator().register(this.generators);
		new NestjsResourceGenerator().register(this.generators);
	}

	static async run(
		schematic: string,
		args: string[],
		options: GenerateCommandOptions,
	) {
		intro("Generate");

		const generatorNames = Object.keys(GenerateCommand.generators);

		const generatorName = await select({
			message: "Select generator",
			options: generatorNames.map((it) => ({ value: it, label: it })),
		});

		if (isCancel(generatorName)) {
			return;
		}

		const generator = GenerateCommand.generators[generatorName];

		await generator.run();

		outro("Finish");
	}
}
