import { Command } from "commander";

import { spawn } from "../helpers/spawn.js";

interface AddCommandOptions {}

export class AddCommand {
	static register(program: Command) {
		program
			.command("add")
			.description("add and install NPM packages")
			.argument("<packages...>", `NPM packages to be added as dependencies`)
			.allowUnknownOption()
			.addHelpText("after", "  options accepted by 'yarn add'")
			.action(AddCommand.run);
	}

	static async run(args: string[], options: AddCommandOptions) {
		await spawn("yarn", ["add", ...args], { stdio: "inherit", shell: true });
	}
}
