import { Command } from "commander";

import { spawn } from "../helpers/spawn.js";

interface UnlinkCommandOptions {
	reinstall: boolean;
}

export class UnlinkCommand {
	static register(program: Command) {
		program
			.command("unlink")
			.description("unlink package(s) for development (host & docker)")
			.argument("[package...]", `name of the package(s)`)
			.option(
				"--no-reinstall",
				"do not reinstall unlinked package (if set as dependency)",
			)
			.action(UnlinkCommand.run);
	}

	static async run(packageNames: string[], options: UnlinkCommandOptions) {
		await spawn("yalc", ["remove", ...packageNames], {
			stdio: "inherit",
			shell: true,
		});
	}
}
