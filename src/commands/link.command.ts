import { Command } from "commander";

import { spawn } from "../helpers/spawn.js";

interface LinkCommandOptions {
	file: boolean | string;
}

export class LinkCommand {
	static register(program: Command) {
		program = program
			.command("link")
			.description("link package(s) for development (host & docker)")
			.argument("[packages...]", `name of the package(s)`)
			.action(LinkCommand.run);
	}

	static async run(packageNames: string[], options: LinkCommandOptions) {
		await spawn("yalc", ["add", "--link", ...packageNames], {
			stdio: "inherit",
			shell: true,
		});
	}
}
