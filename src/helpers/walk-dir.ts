import fs from "fs";
import path from "path";

export async function* walkDir(
	dir: string,
	parentPath: string = "",
): AsyncGenerator<string> {
	for await (const d of await fs.promises.opendir(dir)) {
		const relativePath = path.join(parentPath, d.name);
		yield relativePath;

		if (d.isDirectory()) {
			const absolutePath = path.join(dir, d.name);
			yield* walkDir(absolutePath, relativePath);
		}
	}
}
