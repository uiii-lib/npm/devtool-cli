export function unindent(str: string) {
	const firstIndent = str.match(/^\n(\s+)/)?.[1];

	if (!firstIndent) {
		throw new Error("First line has no indentation");
	}

	return str
		.replace(new RegExp(`\\n\\s{${firstIndent.length}}`, "g"), "\n")
		.replace(/^\n/, "")
		.replace(/\n\s*$/, "");
}
