import fs from "fs/promises";

export async function fileExists(filepath: string) {
	try {
		await fs.stat(filepath);
		return true;
	} catch (e) {
		return false;
	}
}
