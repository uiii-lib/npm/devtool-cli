import {
	spawn as nodeSpawn,
	SpawnOptions as NodeSpawnOptions,
} from "child_process";
import path from "path";
import url from "url";

import { getRootPackageJson } from "./package-json.js";

export interface SpawnOptions extends NodeSpawnOptions {
	elevate?: boolean;
}

export function spawn(
	command: string,
	params: string[] = [],
	options: SpawnOptions = {},
) {
	options = {
		elevate: false,
		...options,
	};

	if (options.elevate) {
		params.unshift(command);
		const packageJson = getRootPackageJson(
			path.dirname(url.fileURLToPath(import.meta.url)),
		);
		command = path.join(packageJson.cwd, "bin", "elevate.cmd");
	}

	const proc = nodeSpawn(command, params, { stdio: "inherit", ...options });

	return new Promise<void>((resolve, reject) => {
		proc.on("close", (code) => {
			if (code) {
				reject(code);
			}

			resolve();
		});
	});
}
