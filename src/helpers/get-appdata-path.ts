import fs from "fs";
import os from "os";
import path from "path";

function getForWindows() {
	return path.join(os.homedir(), "AppData", "Roaming");
}

function getForMac() {
	return path.join(os.homedir(), "Library", "Application Support");
}

function getForLinux() {
	return path.join(os.homedir(), ".config");
}

function getFallback() {
	if (os.platform().startsWith("win")) {
		// Who knows, maybe its win64?
		return getForWindows();
	}
	return getForLinux();
}

export function getAppDataPath() {
	let appDataPath = process.env.APPDATA;

	if (appDataPath === undefined) {
		switch (os.platform()) {
			case "win32":
				appDataPath = getForWindows();
				break;
			case "darwin":
				appDataPath = getForMac();
				break;
			case "linux":
				appDataPath = getForLinux();
				break;
			default:
				appDataPath = getFallback();
				break;
		}
	}

	appDataPath = path.join(appDataPath!, "uiii-lib-devtool-cli");

	if (!fs.existsSync(appDataPath)) {
		fs.mkdirSync(appDataPath, { recursive: true });
	}

	return appDataPath;
}
