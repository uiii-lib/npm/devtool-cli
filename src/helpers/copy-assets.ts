import fs from "fs";
import path from "path";

import { walkDir } from "./walk-dir.js";

export async function copyAssets(from: string, to: string) {
	for await (const file of walkDir(from)) {
		fs.copyFileSync(
			path.join(from, file),
			path.join(to, file.replace(/.template$/, "")),
		);
	}
}
