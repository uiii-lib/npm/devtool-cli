import fs from "fs";
import path from "path";

import { loadJson, saveJson } from "./json.js";

export function setPackageJsonValue(
	cwd: string,
	key: string,
	value: any,
	afterKey?: string,
) {
	const data = loadJson(path.join(cwd, "package.json"));

	const entries = Object.entries(data).filter((it) => it[0] !== key);

	const afterIndex = entries.findIndex((it) => it[0] === afterKey);

	entries.splice(afterIndex > -1 ? afterIndex + 1 : entries.length, 0, [
		key,
		value,
	]);

	saveJson(path.join(cwd, "package.json"), Object.fromEntries(entries));
}

export function getPackageJsonParents(cwd: string) {
	let currentDir = cwd;

	const parents: any[] = [];

	while (true) {
		const packageJson = loadJson(path.join(currentDir, "package.json"));

		if (packageJson) {
			parents.push(packageJson);

			if (packageJson.root) {
				packageJson.cwd = currentDir;
				return parents.reverse();
			}
		}

		const parentDir = path.dirname(currentDir);

		if (parentDir == currentDir) {
			throw new Error("Root package.json cannot be found");
		}

		currentDir = parentDir;
	}
}

export function getRootPackageJson(cwd: string) {
	const parents = getPackageJsonParents(cwd);
	return parents[0];
}
