import {
	ObjectLiteralExpression,
	PropertyAssignment,
	SyntaxKind,
} from "ts-morph";

export function sortPropertyAssignments(
	obj: ObjectLiteralExpression,
	cmp: (a: PropertyAssignment, b: PropertyAssignment) => number,
) {
	const propertyAssignemnts = obj
		.getProperties()
		.map((it) => it.asKindOrThrow(SyntaxKind.PropertyAssignment));

	propertyAssignemnts.sort(cmp);

	const propertyAssignemntStructures = propertyAssignemnts.map((it) => ({
		name: it.getName(),
		initializer: it.getInitializer()?.getText() || "[]",
	}));

	obj.getProperties().forEach((it) => it.remove());

	obj.addPropertyAssignments(propertyAssignemntStructures);
}
