import path from "path";
import url from "url";

import { getRootPackageJson } from "./package-json.js";

export function getAssetsPath(command: string) {
	const packageJson = getRootPackageJson(
		path.dirname(url.fileURLToPath(import.meta.url)),
	);
	return path.join(packageJson.cwd, "src", "assets", command);
}
