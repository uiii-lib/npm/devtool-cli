import { Project } from "ts-morph";

import { log } from "@uiii-lib/prompts";

export function addImportDeclaration(
	source: string,
	what: string,
	from: string,
) {
	const project = new Project();
	const sourceFile = project.createSourceFile("source.ts", source);

	try {
		let importFrom = sourceFile.getImportDeclaration(
			(it) => it.getModuleSpecifierValue() === from,
		);

		if (!importFrom) {
			importFrom = sourceFile.addImportDeclaration({
				moduleSpecifier: from,
				namedImports: [],
			});
		}

		if (!importFrom.getNamedImports().find((it) => it.getName() === what)) {
			importFrom.addNamedImport({
				name: what,
			});
		}
	} catch (e: any) {
		log.error(
			`Cannot add import declaration: ${e.message}\n\nwhat: ${what}\nfrom: ${from}`,
		);
	}

	return sourceFile.getFullText();
}
