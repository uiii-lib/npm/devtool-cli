import fs from "fs/promises";
import path from "path";

export async function saveFile(filepath: string, content: string) {
	await fs.mkdir(path.dirname(filepath), { recursive: true });
	await fs.writeFile(filepath, content, "utf-8");
}
