export function getStackName(packageJson: any) {
	return packageJson.name.replace("@", "").replace("/", "_");
}
