import { Project, SyntaxKind } from "ts-morph";

import { log } from "@uiii-lib/prompts";

import { sortPropertyAssignments } from "./sort-property-assignments.js";

export function addModuleMetadataDeclaration(
	source: string,
	metaProperty: string,
	declaration: string,
) {
	const project = new Project();
	const sourceFile = project.createSourceFile("source.ts", source);

	try {
		const moduleClass = sourceFile
			.getClasses()
			.find(
				(it) => it.getName()?.endsWith("Module") && !!it.getDecorator("Module"),
			);

		if (!moduleClass) {
			throw new Error("No module declaration");
		}

		const decorator = moduleClass.getDecoratorOrThrow("Module");

		const decoratorMeta = decorator
			.getArguments()[0]!
			.asKindOrThrow(SyntaxKind.ObjectLiteralExpression);

		let assignment = decoratorMeta.getProperty(metaProperty);

		if (!assignment) {
			assignment = decoratorMeta.addPropertyAssignment({
				name: metaProperty,
				initializer: "[]",
			});
		}

		const providersArray = assignment
			.asKindOrThrow(SyntaxKind.PropertyAssignment)
			.getInitializerIfKindOrThrow(SyntaxKind.ArrayLiteralExpression);

		if (
			!providersArray.getElements().find((it) => it.getText() === declaration)
		) {
			providersArray.addElement(declaration);
		}

		sortPropertyAssignments(decoratorMeta, (a, b) => {
			const order = ["imports", "providers", "contollers", "export"];

			const aIndex = order.indexOf(a.getName());
			const bIndex = order.indexOf(b.getName());

			return bIndex - aIndex;
		});
	} catch (e: any) {
		log.error(
			`Cannot add module metadata declaration: ${e.message}\n\nproperty: ${metaProperty}\ndeclaration: ${declaration}`,
		);
	}

	return sourceFile.getFullText();
}
