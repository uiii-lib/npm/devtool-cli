import * as prettier from "prettier";

export async function prettify(content: string, filePath: string) {
	const prettierConfig = (await prettier.resolveConfig(filePath)) || {};

	content = await prettier.format(content, {
		filepath: filePath,
		...prettierConfig,
	});

	return content;
}
