import fs from "fs/promises";

export async function convertEolToLf(filepath: string) {
	const content = await fs.readFile(filepath, "utf-8");
	await fs.writeFile(filepath, content.replace(/\r\n/g, "\n"), "utf-8");
}
