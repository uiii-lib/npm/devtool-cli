import fs from "fs";
import stripBom from "strip-bom";
import stripJsonComments from "strip-json-comments";

export function loadJson(filepath: string) {
	if (!fs.existsSync(filepath)) {
		return false;
	}

	return JSON.parse(
		stripBom(stripJsonComments(fs.readFileSync(filepath, "utf-8"))),
	);
}

export function saveJson(filepath: string, data: any) {
	return fs.writeFileSync(filepath, JSON.stringify(data, null, 2));
}
