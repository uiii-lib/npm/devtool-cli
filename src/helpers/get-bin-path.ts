import fs from "fs";
import path from "path";
import url from "url";

import { getRootPackageJson } from "./package-json.js";

export function getBinPath(binFilename: string) {
	const packageJson = getRootPackageJson(
		path.dirname(url.fileURLToPath(import.meta.url)),
	);
	const binDir = path.join(packageJson.cwd, "bin");
	const binPath = path.join(binDir, binFilename);

	if (!fs.existsSync(binPath)) {
		throw new Error(`Binary '${binPath}' doesn't exist`);
	}

	return binPath;
}
