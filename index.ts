#!/usr/bin/env node
import { program } from "commander";

import { AddCommand } from "./src/commands/add.command.js";
import { BashCommand } from "./src/commands/bash.command.js";
import { BuildImagesCommand } from "./src/commands/build-images.command.js";
import { BuildCommand } from "./src/commands/build.command.js";
import { ConfigCommand } from "./src/commands/config.command.js";
import { CreateCommand } from "./src/commands/create.command.js";
import { GenerateCommand } from "./src/commands/generate.command.js";
import { InstallCommand } from "./src/commands/install.command.js";
import { LinkCommand } from "./src/commands/link.command.js";
import { LogsCommand } from "./src/commands/logs.command.js";
import { RestartCommand } from "./src/commands/restart.command.js";
import { StartCommand } from "./src/commands/start.command.js";
import { StopCommand } from "./src/commands/stop.command.js";
import { UnlinkCommand } from "./src/commands/unlink.command.js";

async function main() {
	program.name("dev");

	AddCommand.register(program);
	BashCommand.register(program);
	BuildCommand.register(program);
	BuildImagesCommand.register(program);
	ConfigCommand.register(program);
	CreateCommand.register(program);
	GenerateCommand.register(program);
	InstallCommand.register(program);
	LinkCommand.register(program);
	LogsCommand.register(program);
	RestartCommand.register(program);
	StartCommand.register(program);
	StopCommand.register(program);
	UnlinkCommand.register(program);

	await program.parseAsync();
}

main();
